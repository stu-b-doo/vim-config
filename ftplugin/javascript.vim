" ftplugin vim javascript
" separate file for jsx file types

" show existing tab with n spaces width
set tabstop=2
" when indenting with '>', use n spaces width
set shiftwidth=2
" On pressing tab, insert n spaces. see option noexpandtab
set expandtab
